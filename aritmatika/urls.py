from django.urls import path


from . import views

urlpatterns = [
    path('perkalian', views.perkalian, name='perkalian'),
    path('penjumlahan', views.penjumlahan, name='penjumlahan'),
    path('pemangkatan', views.pemangkatan, name='pemangkatan'),
]