from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

# Create your views here.

@csrf_exempt
def perkalian(request):
    if request.method == 'POST':
        angka1 = request.POST['angka1']
        angka2 = request.POST['angka2']
        hasildict_ = {}
        hasildict_['hasil'] = int(angka1) * int(angka2)
        return JsonResponse(hasildict_, safe=False)
    return HttpResponse('untuk melakukan perkalian, gunakan method post')

@csrf_exempt
def penjumlahan(request):
    if request.method == 'POST':
        angka1 = request.POST['angka1']
        angka2 = request.POST['angka2']
        hasildict_ = {}
        hasildict_['hasil'] = int(angka1) + int(angka2)
        return JsonResponse(hasildict_, safe=False)
    return HttpResponse('untuk melakukan penjumlahan, gunakan method post')

@csrf_exempt
def pemangkatan(request):
    if request.method == 'POST':
        angka1 = request.POST['angka1']
        angka2 = request.POST['angka2']
        hasildict_ = {}
        hasildict_['hasil'] = int(angka1) ** int(angka2)
        return JsonResponse(hasildict_, safe=False)
    return HttpResponse('untuk melakukan pemangkatan, gunakan method post')